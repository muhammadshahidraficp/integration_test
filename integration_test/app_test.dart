import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

import 'automation_utility.dart';

void main() {
  group('hello world test', () {
    IntegrationTestWidgetsFlutterBinding.ensureInitialized();

    testWidgets("app full test", (WidgetTester tester) async {
      await AutomationUtility().loadApp(tester);
      await AutomationUtility().futureDelay(tester, 10);
      for (var i = 0; i < 5; i++) {
        await AutomationUtility()
            .findWidgetByKeyAndTap(tester, Key('add_button'));
        await AutomationUtility().futureDelay(tester, 2);
      }
    });
  });
}
