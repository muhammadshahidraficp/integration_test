import 'dart:async';
import 'package:flutter/material.dart';
import 'package:hello/main.dart' as app;
import 'package:flutter_test/flutter_test.dart';

class AutomationUtility {
  futureDelay(WidgetTester tester, seconds) async {
    bool timerDone = false;
    Timer(Duration(seconds: seconds), () => timerDone = true);
    while (timerDone != true) {
      await tester.pump();
    }
  }

  loadApp(WidgetTester tester) async {
    app.main();
    await tester.pumpAndSettle();
  }

  findWidgetByKeyAndTap(WidgetTester tester, Key key) async {
    var field = find.byKey(key).first;
    print(field);
    await tester.tap(field);
    await futureDelay(tester, 15);
  }

  findWidgetByKeyAndEnterText(WidgetTester tester, Key key, String data) async {
    var field = find.byKey(key).first;
    print(field);
    await tester.enterText(field, data);
    await futureDelay(tester, 15);
  }
}
